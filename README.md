# Spherical_Path_Comparison
Spherical Path Comparison (spComparison) Package is developed for quantitatively
measuring similarity of spherical paths, especially the paleomagnetic apparent
polar wander paths (APWPs) of tectonic plates. It is powered by GMT and PmagPy.
Read on for more details here: https://github.com/f-i/APWP_similarity

# About the Functions
Because of complexity of the related algorithms about spherical surface geometry,
old-version directional change for each segment (seg) was actually the dif
between its previous seg and itself (if we think about it, this solution should
be able to give close results to dif between the 1st seg and itself). The
corresponding function is "spa_angpre_len_dif". This solution is also relatively
faster. I've already figured out a solution for calculating dif between each
segment and always the 1st seg, which is the function "spa_ang1st_len_dif".
However, unfortunately its execution time is much more (about 5 times) than the
previous solution. Working on the code efficiency but not very positive about
the improvement. Maybe we need to balance complexity and time. I'm also
considering to write all codes in C language for the following chapters.

* Complexity 1: Angle between two DIRECTIONAL geodesics (i.e. segments which
  are with DIRECTIONs in the order of poles' ages, but not necessarily
  successive displacement ones). Angle between two geodesics (no constraints
  on their directions) could be 2 solutions at both intersections of the
  great circles that the two geodesics are on. However, if the two geodesics
  have directions, the correct angle between them would be just one of the
  above 2 solutions.

# Related Algorithms

## About Segment Angle Difference

![](https://bitbucket.org/Su-Chenjian/spherical_path_comparison/raw/master/fig1directionalGeodesics.png)

![](https://bitbucket.org/Su-Chenjian/spherical_path_comparison/raw/master/fig1caption.png)
